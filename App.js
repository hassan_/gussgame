import React, { useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';
import Header from './components/Header';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen';
import GameOverScreen from './screens/GameOverScreen';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans-Regular': require('./assets/fonts/ProductSansRegular.ttf'),
    'open-sans-Bold': require('./assets/fonts/ProductSansBold.ttf'),
    'open-sans-BoldItalic': require('./assets/fonts/ProductSansBoldItalic.ttf'),
    'open-sans-Italic': require('./assets/fonts/ProductSansItalic.ttf'),
  });
};

export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [guessRounds, setGuessRounds] = useState(0);
  const [dataLoaded, setDataLoaded] = useState(false);

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
        onError={(err) => console.log(err)}
      />
    )
  }

  const startGameHandler = (selectNumber) => {
    setUserNumber(selectNumber);
    setGuessRounds(0);
  };

  const gameOverHandler = (numberOfRounds) => {
    setGuessRounds(numberOfRounds);
  }

  const configureNewGameHangler = () => {
    setGuessRounds(0);
    setUserNumber(null);
  }

  let content = <StartGameScreen onStartGame={startGameHandler} />;

  if (userNumber && guessRounds <= 0) {
    content = <GameScreen userChoice={userNumber} onGameOver={gameOverHandler} />;
  } else if (guessRounds > 0) {
    content = <GameOverScreen guessRounds={guessRounds} userNumber={userNumber} onRestart={configureNewGameHangler} />;
  }
  return (
    <SafeAreaView style={styles.screen}>
      <Header title="Guess Number Game" />
      {content}
    </SafeAreaView>
  );


}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
});
