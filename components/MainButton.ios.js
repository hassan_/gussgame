import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Colors from '../constants/Colors';

const MainButton = props => {
   
    return(
        <TouchableOpacity onPress={props.onPress}>
        <View style={styles.btn}>
            <Text style={styles.buttonText}>{props.children}</Text>
        </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    btn:{
      backgroundColor: Colors.primary,
      paddingVertical:12,
      paddingHorizontal:30,
      borderRadius:25,
      alignItems:'center',
    },
    buttonText:{
     color: 'white',
     fontFamily: 'open-sans-Bold',
     fontSize:20,
    },
});
export default MainButton;