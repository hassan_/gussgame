import React, { useState, useEffect} from 'react';
import { View, Text, StyleSheet, Button,
     TouchableWithoutFeedback, Keyboard, Alert, Dimensions, ScrollView, KeyboardAvoidingView } from 'react-native';
import Card from '../components/Card';
import Colors from '../constants/Colors';
import Input from '../components/Input';
import BodyText from '../components/BodyText';
import TitleText from '../components/TitleText';
import NumberContainer from '../components/NumberContainer';
import MainButton from '../components/MainButton';
import GlobalStyle from '../constants/GlobalStyle';

const StartGameScreen = props => {
    const [enterValue, setEnterValue] = useState('');
    const [confirmed, setConfirmed] = useState(false);
    const [enterNumber, setEnternumber] = useState('');
    const [buttonWidth,setButtonWidth] = useState(Dimensions.get('window').width /4);

    const numberInputHandler = (inputText) => {
        setEnterValue(inputText.replace(/[^0-9]/g, ''))
    }

    const resetInputHandler = () => {
        setEnterValue('');
        setConfirmed(false);
    };

    useEffect(() =>{
        const updateLayout = () =>{
            setButtonWidth(Dimensions.get('window').width /4);
        };
        Dimensions.addEventListener('change', updateLayout);

        return() =>{
            Dimensions.removeEventListener('change', updateLayout);
        };

    });

    const confirmInputHandler = () => {
        const chosenNumber = parseInt(enterValue);
        if(isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99){
            Alert.alert(
                'Enter Number',
                'Number Has To Be A Number Between 1 To 99',
                [{text: 'Okey', style: 'cancel', onPress: resetInputHandler }]
            )
            return;
        }
        setConfirmed(true);
        setEnternumber(enterValue);
        setEnterValue('');
        Keyboard.dismiss();
    };

    
    let confirmedOutput;
    if(confirmed){
        confirmedOutput = (
            <Card style={styles.numberContainer}>
                <Text style={GlobalStyle.bodyTitle}>Your Selected</Text>
                <NumberContainer>{enterNumber}</NumberContainer>
                <MainButton  onPress={() => props.onStartGame(enterNumber)}>
                  Game Start
                </MainButton>
            </Card>
        ); 
    }

    return (
        <ScrollView>
         <KeyboardAvoidingView behavior="height" KeyboardVerticalOffSet={20}>
         {/* behavior="position" bestforandroid(padding) bestforios(position)  height  */}
        <TouchableWithoutFeedback onPress={() => {
            Keyboard.dismiss();
        }}>
            <View style={styles.screen}>
                <TitleText style={styles.title}>Start Game</TitleText>
                <Card style={styles.inputContainer}>
                    <BodyText>Select a Number</BodyText>
                    <Input
                        onChangeText={numberInputHandler}
                        value={enterValue}
                        style={styles.input}
                        blurOnSubmit
                        //autoCapitalize={false}
                        maxLength={2}
                        keyboardType="number-pad" />
                    <View style={styles.buttonContainer}>
                        <View style={{width:buttonWidth}}>
                            <Button  title="Reset" color={Colors.accent} onPress={resetInputHandler} />
                        </View>
                        <View style={{width:buttonWidth}}>
                            <Button  title="Confirm" color={Colors.primary} onPress={confirmInputHandler} />
                        </View>
                    </View>
                </Card>
                {confirmedOutput}
            </View>
        </TouchableWithoutFeedback>
        </KeyboardAvoidingView>  
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        marginVertical: 10,
    },
    inputContainer: {
        width: '80%',
        maxWidth:'95%',
        minWidth: 300,
        alignItems: 'center',
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
    },
    button: {
        //width: 100,
        width: Dimensions.get('window').width / 4,
    },
    input: {
        width: 40,
        textAlign: 'center',
    },
    numberContainer:{
        marginTop:20,
        alignItems:'center',
    }
});

export default StartGameScreen;