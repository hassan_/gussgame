import React from 'react';
import { Text, View, StyleSheet, Button, Image,Dimensions, ScrollView } from 'react-native';
import BodyText from '../components/BodyText';
import TitleText from '../components/TitleText';
import Colors from '../constants/Colors';
import MainButton from '../components/MainButton';
const GameOverScreen = props => {
    return (
        <ScrollView>
        <View style={styles.screen}>
            <TitleText>Game is Over!</TitleText>
            <View style={styles.imgContainer}>
                <Image
                    fadeDuration={1000}
                    source={require('../assets/img/gameover.png')}
                    style={styles.img}
                //source={{uri:'https://www.google.com/search?q=game+image&hl=en&sxsrf=ALeKk01-aZJOzShfcsge7LhOo6KOxx5hqw:1584536849647&tbm=isch&source=iu&ictx=1&fir=gSOe4fVKWUWyzM%253A%252CcwRzOseMpfBjQM%252C_&vet=1&usg=AI4_-kTAPDKhN9gGmyOZxw91L7ABDGTZrQ&sa=X&ved=2ahUKEwiJ37ngi6ToAhUzDmMBHY8wCB0Q9QEwAHoECAkQMA#imgrc=gSOe4fVKWUWyzM:'}}
                //resizeMode="center" cover center contain repeat stretch
                />
            </View>

            <View style={styles.resultContainer}>
                <BodyText style={styles.resultText}>Your Phone Needed
                <Text style={styles.highlighting}> {props.guessRounds} </Text>
                    Rounds To Guess Your Given Number <Text style={styles.highlighting}>{props.userNumber}</Text>
                </BodyText>
                <View style={styles.newGameBtn} >
                    <MainButton onPress={props.onRestart}>
                        New Game!
                </MainButton>
                </View>
            </View>

        </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({

    screen: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgContainer: {
        width:Dimensions.get('window').width * 0.7,
        height:Dimensions.get('window').width * 0.7,
        borderRadius:Dimensions.get('window').width * 0.7 / 2,
        // borderRadius is half of box the width and height
        borderWidth: 3,
        borderColor: 'black',
        overflow: 'hidden',
        marginVertical: Dimensions.get('window').height /30,
    },
    img: {
        width: '100%',
        height: '100%',
    },
    resultContainer: {
        marginVertical: Dimensions.get('window').height /60,
        marginHorizontal: 30
    },
    resultText: {
        textAlign: 'center',
        fontSize: Dimensions.get('window').height < 400 ? 10 : 20,
    },
    highlighting: {
        color: Colors.primary,
        fontFamily: 'open-sans-Regular'
    },
    newGameBtn: {
        marginTop: 20,
    }

});

export default GameOverScreen;