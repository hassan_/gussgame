import React, { useState, useRef, useEffect } from 'react';
import {View, Text, StyleSheet, Button, Alert, Dimensions, ScrollView, FlatList} from 'react-native';
import NumberContainer from '../components/NumberContainer';
import Card from '../components/Card';
import Colors from '../constants/Colors';
import MainButton from '../components/MainButton';
import { Ionicons } from '@expo/vector-icons';
import {ScreenOrientation} from 'expo';

const generateRandomBetween = (min, max, exclude) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    const randNum = Math.floor(Math.random() * (max - min)) + min;
    if (randNum === exclude) {
        const check = generateRandomBetween(min, max, exclude);
        return generateRandomBetween(min, max, exclude);
    } else {
        return randNum;
    }
}

//ScrolView
// const renderListItem = (passGuess,roundNum) => (
//     <View key={passGuess} style={styles.listItems}>
//         <Text>#{roundNum}</Text>
//         <Text>{passGuess}</Text>
//     </View>
// );

//FlatList
const renderListItem = (listLength, itemData) => (
    <View style={styles.listItems}>
        <Text>#{listLength - itemData.index}</Text>
        <Text>{itemData.item}</Text>
    </View>
);

//render different layout accounding to dimensions or dofferent devices
// if(Dimensions.get(window).height > 600){
//     return <View></View>
// }

const GameScreen = props => {
    // used for lock app PORTRAIT mode
    //ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT); 
    const initialGuess = generateRandomBetween(1, 100, props.userChoice);
    const [currentGuess, setCurrentGuess] = useState(initialGuess);
    const [passGuess, setPassGuess] = useState([initialGuess.toString()]);
    const [availableDeviceWidth, setAvailableDeviceWidth] = useState(Dimensions.get('window').width);
    const [availableDeviceHeight, setAvailableDeviceHeight] = useState(Dimensions.get('window').height);
    const [rounds, setRounds] = useState(0);
    const currentLow = useRef(1);
    const currentHigh = useRef(100);
    const { userChoice, onGameOver } = props;

    useEffect(() =>{
        const updateLayout = () =>{
            setAvailableDeviceWidth(Dimensions.get('window').width);
            setAvailableDeviceHeight(Dimensions.get('window').height);
        };
        Dimensions.addEventListener('change', updateLayout);
        return() =>{
            Dimensions.removeEventListener('change', updateLayout);
        };
    });

    useEffect(() => {
        if (currentGuess == userChoice) {
            onGameOver(passGuess.length);
        }
    }, [currentGuess, userChoice, onGameOver]);

    const nextGuessHandler = direction => {
        if (
            (direction === 'lower' && currentGuess < props.userChoice) ||
            (direction === 'greater' && currentGuess > props.userChoice)
        ) {
            Alert.alert(
                "Don't lie!",
                "You know that this is wrong...",
                [{ text: 'Sorry!', style: 'cancel' }]);
            return;
        }
        if (direction === 'lower') {
            currentHigh.current = currentGuess;
        } else {
            currentLow.current = currentGuess + 1;
        }
        const nextNumber = generateRandomBetween(currentLow.current, currentHigh.current, currentGuess);
        setCurrentGuess(nextNumber);
        // setRounds(curRounds => curRounds + 1 );
        setPassGuess(currGuess => [nextNumber.toString(), ...currGuess]);
    }

    //Dynamic Style class Assign
    let listContainerStyle = styles.listContainer;
    if (availableDeviceWidth < 350) {
        listContainerStyle = styles.listContainerBig;
    }

    if (availableDeviceHeight < 500){
        return (
            <View style={styles.screen}>
            <Text>Guess Here</Text>
            <View style={styles.controls}>
            <MainButton color={Colors.accent} onPress={nextGuessHandler.bind(this, 'lower')}>
                    <Ionicons name="md-remove" size={25} color="white" />
            </MainButton>
            <NumberContainer>{currentGuess}</NumberContainer>
            <MainButton color={Colors.primary} onPress={nextGuessHandler.bind(this, 'greater')}>
                    <Ionicons name="md-add" size={25} color="white" />
            </MainButton>
            </View>
            <View style={styles.listContainer}>
                <FlatList
                    keyExtractor={(item) => item}
                    data={passGuess}
                    renderItem={renderListItem.bind(this, passGuess.length)}
                    contentContainerStyle={styles.list}
                />
            </View>
        </View>
        );
    }

    return (
        <View style={styles.screen}>
            <Text>Guess Here</Text>
            <NumberContainer>{currentGuess}</NumberContainer>
            <Card style={styles.buttonContainer}>
                <MainButton color={Colors.accent} onPress={nextGuessHandler.bind(this, 'lower')}>
                    <Ionicons name="md-remove" size={25} color="white" />
                </MainButton>
                <MainButton color={Colors.primary} onPress={nextGuessHandler.bind(this, 'greater')}>
                    <Ionicons name="md-add" size={25} color="white" />
                </MainButton>
            </Card>
            <View style={styles.heading}>
                <View style={styles.headingItem}>
                    <Text style={styles.headingText}>Rounds</Text>
                    <Text style={styles.headingText} >Guesses</Text>
                </View>
            </View>
            <View style={styles.listContainer}>
                {/* <ScrollView  contentContainerStyle={styles.list}>
                {passGuess.map( (guess, index) => renderListItem(guess, passGuess.length - index))}
            </ScrollView> */}

                <FlatList
                    keyExtractor={(item) => item}
                    data={passGuess}
                    renderItem={renderListItem.bind(this, passGuess.length)}
                    contentContainerStyle={styles.list}
                />
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: 'center',
        paddingVertical:10,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        // marginTop: Dimensions.get('window').height /50,
        marginTop: Dimensions.get('window').height > 600 ? 20 : 5, //if check
        width: 400,
        maxWidth: '90%',
    },
    heading: {
        width: Dimensions.get('window').width > 350 ? '80%' : '90%',
    },
    headingItem: {
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#ccc',
        borderWidth: 3,
        borderRadius: 10,
        padding: 10,
        marginVertical: 10,
    },
    headingText: {
        fontFamily: 'open-sans-Bold',
        fontSize: 18,
        color: '#00050D',
    },
    controls:{
        width: '80%',
        flexDirection: 'row',
        justifyContent:'space-around',
        alignItems:'center',
    },
    listContainer: {
        flex: 1,
        width:Dimensions.get('window').width > 350 ? '80%' : '90%', //if check
        //width: '80%'
    },
    listContainerBig: {
        flex: 1,
        width: '90%',
    },
    list: {
        flexGrow: 1,
        // alignItems:'center',
        justifyContent: 'flex-end'
    },
    listItems: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 10,
        padding: 15,
        marginVertical: 10,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    }
});
export default GameScreen;